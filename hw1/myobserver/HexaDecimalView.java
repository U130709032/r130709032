package myobserver;

public class HexaDecimalView extends Observer{
	public HexaDecimalView(NumberPublisher npublisher){
      this.npublisher = npublisher;
      this.npublisher.publishNumber(this);
   }
  
   
	public void update(int num) {
		display(num);
		
	}
	
	public void display(int num){
		System.out.println(Integer.toHexString(num));
	}
}
