package myobserver;

public abstract class Observer {

   protected BinaryView bView;
   protected HexaDecimalView hView;
   protected DecimalView dView;;
   public abstract void update();
}