package myobserver;

public class DecimalView extends Observer{
	public DecimalView(NumberPublisher npublisher){
      this.npublisher = npublisher;
      this.npublisher.publishNumber(this);
   }
	
	public void update(int num) {
		display(num);
		
	}
	
	public void display(int num){
		System.out.println(num);
	}
}
