package myobserver;

public class BinaryView extends Observer {
	 public BinaryView(NumberPublisher npublisher){
      this.npublisher = npublisher;
      this.npublisher.publishNumber(this);
   }

	

	public void update(int num) {
		display(num);
		
	}
	
	public void display(int num){
		System.out.println(Integer.toBinaryString(num));
	}

}
