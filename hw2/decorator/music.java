package decorator;

import decorator.Decorator;
import decorator.automobiles;;

public class music extends Decorator {

	
	
	public music(automobiles decoratedCar) {
		super(decoratedCar);
	}
	
	@Override
	public float cost() {
		return (float) 1000 + decoratedCar.cost();
	}

	@Override
	public String getDescription() {
		
		return decoratedCar.getDescription() + " Music";
	}

}