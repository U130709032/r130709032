package decorator;

import java.util.ArrayList;
import java.util.List;

public class AutoDemo {

	public static void main(String[] args) {
		
		List<automobiles> items = new ArrayList<>();
		automobiles sity = new Sity();
		sity = new abs(sity); 
		sity = new music(sity);
		sity = new sunroof(sity);
		items.add(sity);
		automobiles sivic = new Sivic();
		sivic = new abs(sivic);
		sivic = new airbag(sivic);
		items.add(sivic);

		for(automobiles a: items){
			System.out.println("cost " + a.getDescription() + 
					" is " + a.cost());			
		}
		
		
	}

}
