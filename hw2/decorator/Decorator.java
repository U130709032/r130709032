package decorator;

import decorator.automobiles;

public abstract class Decorator extends automobiles {

	automobiles decoratedCar;
	
	public Decorator(automobiles decoratedCar) {
		this.decoratedCar = decoratedCar;
	}
	
	public abstract String getDescription();
	
}
