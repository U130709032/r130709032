package decorator;


import decorator.Decorator;
import decorator.automobiles;;

public class airbag extends Decorator {

	
	
	public airbag(automobiles decoratedCar) {
		super(decoratedCar);
	}
	
	@Override
	public float cost() {
		return (float) 3000 + decoratedCar.cost();
	}

	@Override
	public String getDescription() {
		
		return decoratedCar.getDescription() + " Airbag";
	}

}